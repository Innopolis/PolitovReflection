package innopolis.politov;

import java.io.Externalizable;

/**
 * Created by General on 2/10/2017.
 */
public class People {
    private String name;
    private Integer age;
    private Double salary;

    public People() {
    }

    protected void  paySalary(){
        System.out.println("Salary" + salary);

    }

    public People(String name, Integer age, Double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getSlary() {
        return salary;
    }

    public void setSlary(Double slary) {
        this.salary = slary;
    }
}
