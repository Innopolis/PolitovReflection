package innopolis.politov;

import java.io.*;
import java.util.Hashtable;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by General on 2/15/2017.
 */
public class JarClassLoader extends ClassLoader {
    private String jarFile;//Path to the jar file
    private Hashtable classes = new Hashtable(); //used to cache already defined classes
    private String localJarFile = "temp.jar";

    public JarClassLoader() {
        super(JarClassLoader.class.getClassLoader()); //calls the parent class loader's constructor
    }

    public JarClassLoader(String pathToJar){
        this();
        jarFile = pathToJar;
    }

    public Class loadClass(String className) throws ClassNotFoundException {
        return findClass(className);
    }

    public Class findClass(String className) {
        byte classByte[];
        Class result = null;

        result = (Class) classes.get(className); //checks in cached classes
        if (result != null) {
            return result;
        }

        try {
            return findSystemClass(className);
        } catch (Exception e) {
        }

        try {
            String strURL = jarFile;

            loadJarFileToLocalDir(jarFile);
            JarFile jar = new JarFile(localJarFile);
            JarEntry entry = jar.getJarEntry(className + ".class");
            InputStream is = jar.getInputStream(entry);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            int nextValue = is.read();
            while (-1 != nextValue) {
                byteStream.write(nextValue);
                nextValue = is.read();
            }

            classByte = byteStream.toByteArray();
            result = defineClass(className, classByte, 0, classByte.length, null);
            classes.put(className, result);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    private void loadJarFileToLocalDir(String pathToJar){
        try {
            InputStream in = null;
            in = CreateAbstractInputStream
                    .createInputStream(pathToJar);
            OutputStream writer = new FileOutputStream(localJarFile);
            byte buffer[] = new byte[1000000];
            int c = in.read(buffer);
            while (c > 0) {
                writer.write(buffer, 0, c);
                c = in.read(buffer);
            }
            writer.flush();
            writer.close();
            in.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
