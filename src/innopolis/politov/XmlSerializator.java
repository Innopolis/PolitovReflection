package innopolis.politov;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.TransformerConfigurationException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by General on 2/10/2017.
 */
public class XmlSerializator {

    public static void serialize(Object serObj, String filePath){

        Document xmlDocument = XmlDocument.createDocument();
        Class classForSerializable = serObj.getClass();

        //основной тег документа
        Element headElement = xmlDocument.createElement("object");
        headElement.setAttribute("type", classForSerializable.getTypeName());
        xmlDocument.appendChild(headElement);

        //сериализация полей объекта
        for (Field field :
                classForSerializable.getDeclaredFields()
        ){
            field.setAccessible(true);
            Class fieldClass = field.getClass();
            Element childElement = xmlDocument.createElement("field");
            childElement.setAttribute("type", field.getType().getTypeName
                    ());
            childElement.setAttribute("id", field.getName());
            try {
                childElement.setAttribute("value", String.valueOf(field.get(serObj)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            headElement.appendChild(childElement);
        }
        try {
            XmlDocument.writeDocument(xmlDocument, filePath);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }

    }

    public static Object deserialize(String filePath, ClassLoader customLoader)
            throws
            ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        Document xmlDocument = XmlDocument.createDocumentForParse(filePath);

        Node firstNode  = xmlDocument.getFirstChild();
        String className = firstNode.getAttributes()
                .getNamedItem("type").getNodeValue();
        Class readedClassObject;
        try{
            readedClassObject = Class.forName(className);
        }
        catch (ClassNotFoundException e){
            if (customLoader !=null) {
                readedClassObject = customLoader.loadClass(className);
            }
            else throw e;
        }
        Object result = readedClassObject.newInstance();

        NodeList nodeFields =xmlDocument.getElementsByTagName("field");
        Field[] fields = readedClassObject.getDeclaredFields();
        if (nodeFields.getLength()!=  fields.length){
            System.out.println("Не совпадают количество полей сериализуемого " +
                    "класса" );
            return null;
        }

        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
        }

        for (int i = 0; i <fields.length; i++) {
            Node nodeFiled = nodeFields.item(i);

            Class fieldClass = Class.forName( nodeFiled.getAttributes().getNamedItem
                    ("type").getNodeValue());
            String fieldName = nodeFiled.getAttributes().getNamedItem
                    ("id").getNodeValue();

            String fieldValue = nodeFiled.getAttributes().getNamedItem
                    ("value").getNodeValue();

            fieldSetValue(result,fields, fieldName, fieldClass, fieldValue);
        }
        return  result;
    }

    private  static void fieldSetValue(Object result, Field[] fields, String
            fieldName, Class
            fieldClass, String fieldValue){
        for (Field field :fields) {
            if (field.getName().equals(fieldName)) {
                if( field.getType()!= String.class) {
                    try {
                        field.set(result, XmlSerializator.fieldSetValueByExecuteValueOf
                                (fieldClass,
                                        fieldValue));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    try {
                        field.set(result, fieldValue);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private static Object fieldSetValueByExecuteValueOf(Class fieldClass, String value){
        for (Method method :
                fieldClass.getDeclaredMethods()) {
            if (method.getName().equals("valueOf")){
                if ((method.getParameterTypes().length == 1)
                        && ((method.getParameterTypes()[0] == String.class)))
                    try {
//                        Object obj = fieldClass.newInstance();
                        return method.invoke(fieldClass,value );
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

            }
        }
        return null;
    }

}
