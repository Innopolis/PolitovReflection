package innopolis.politov;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;


/**
 * Created by General on 2/8/2017.
 */
public class CreateAbstractInputStream {

    private static boolean isURL (String name){
        return name.matches("[Hh][Tt][Tt][Pp][Ss]?:\\/\\/.+");
    }

    public static InputStream createInputStream(String sourcePath) throws IOException {
        if (isURL(sourcePath)) {
                return new URL(sourcePath).openStream();

        }
        else {
                return new FileInputStream(sourcePath);
        }
    }
}
