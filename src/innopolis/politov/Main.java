package innopolis.politov;

public class Main {

    public static void main(String[] args) throws Exception {

        JarClassLoader jarClassLoader = new JarClassLoader("https://gitlab.com/Innopolis/PolitovAnimal/raw/52a8f2a0be7c7eff9ee37473eb1213b4e4cfaf43/out/artifacts/Animal_jar/Animal.jar" );

        Class classAnimal = jarClassLoader.loadClass("Animal");
        if (classAnimal == null)
            throw new Exception("Класс Animal не загружен/ не найден");
        Object object = classAnimal.newInstance();
        System.out.println("Получен объект загруженного класса " + object
                .getClass().getName());
        XmlSerializator.serialize(object,  "Animal.xml");
        System.out.println("Объект типа " + object.getClass().getName() + " " +
                "сериализован");
        Object deserializedObject = XmlSerializator.deserialize("Animal" +
                ".xml", jarClassLoader);
        System.out.println("Объект типа " + deserializedObject.getClass().getName() + " " +
                "десериализован");

        People people = new People("Vyacheslav", 23, 50000D);
        try {
            people.getClass().getConstructor();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        XmlSerializator.serialize(people, "people.xml");
        People people1 = (People) XmlSerializator.deserialize("people.xml",
                null);

        System.out.println( "People1");
        System.out.println( "Name: " + people1.getName());
        System.out.println( "Age: " + people1.getAge());
        System.out.println("Salary: " + people1.getSlary());
    }



}
